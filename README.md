Improved WoTLK Party Ability Bars
================================

Edit by Nerdox, [fork](https://github.com/wotlk-addons/PartyAbilityBars) from Vendethiel and Lawz. Original by Kollektiv.

### Instructions

Don't forget to :
- Rename the folder from wotlk-partyabilitybars-master to PAB
- If you used an older version of PAB, delete former configuration files : WTF/AccountName/SavedVariables/PAB.lua
- Type `/pab` in-game to open the settings.

### Notes

This version adds a better configuration UI, more spells, and automatic talents detection (as well as an internal cleanup).

![Imgur](https://i.imgur.com/WLFR3d3.jpg) ![Imgur](https://i.imgur.com/x6TA6LA.jpg)

![Imgur](https://i.imgur.com/bmAwt9Z.jpg) ![Imgur](https://i.imgur.com/s5LyxVt.jpg)
